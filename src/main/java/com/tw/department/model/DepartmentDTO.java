package com.tw.department.model;

import com.tw.department.entity.Department;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class DepartmentDTO {

    private Department department;
    private List<EmployeeDTO> employees;

}
