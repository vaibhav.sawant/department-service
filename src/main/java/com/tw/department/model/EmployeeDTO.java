package com.tw.department.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDTO {

    private Integer employeeId;

    private String name;

    private Integer departmentId;

}
