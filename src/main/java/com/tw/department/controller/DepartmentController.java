package com.tw.department.controller;

import com.tw.department.config.EmployeeServiceProperties;
import com.tw.department.entity.Department;
import com.tw.department.exception.DepartmentNotFound;
import com.tw.department.http.RestApiClient;
import com.tw.department.model.DepartmentDTO;
import com.tw.department.model.EmployeeDTO;
import com.tw.department.repository.DepartmentRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/departments")
public class DepartmentController {

    private DepartmentRepository departmentRepository;

    private EmployeeServiceProperties employeeServiceProperties;

    private RestApiClient restApiClient;

    @GetMapping
    public List<DepartmentDTO> getAllDepartments() {
        log.info("Getting all departments");
        List<Department> departments = departmentRepository.findAll();
        return departments
            .stream()
            .map(department -> {
                Map<String, Object> uriVariables = Map.of("departmentId", department.getDepartmentId());

                ResponseEntity<List<EmployeeDTO>> employees = restApiClient.callRestApi(
                        employeeServiceProperties.getGetByDeptId(),
                        HttpMethod.GET,
                        uriVariables,
                        null
                    );

                return new DepartmentDTO(department, employees.getBody());
            })
            .collect(Collectors.toList());
    }

    @GetMapping("/by-id/{departmentId}")
    public DepartmentDTO getDepartmentById(@PathVariable Integer departmentId) {
        log.info("Getting department by id {}", departmentId);
        return departmentRepository
            .findById(departmentId)
            .map(department -> {
                Map<String, Object> uriVariables = Map.of("departmentId", departmentId);

                ResponseEntity<List<EmployeeDTO>> employees = restApiClient.callRestApi(
                    employeeServiceProperties.getGetByDeptId(),
                    HttpMethod.GET,
                    uriVariables,
                    null
                );

                return new DepartmentDTO(department, employees.getBody());
            })
            .orElseThrow(() -> new DepartmentNotFound(departmentId));
    }

    @Transactional
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DepartmentDTO addDepartment(@RequestBody DepartmentDTO departmentDTO) {
        log.info("Saving department: {}", departmentDTO);
        Department department = departmentRepository.save(departmentDTO.getDepartment());

        departmentDTO.getEmployees()
            .stream()
            .peek(employeeDTO -> employeeDTO.setDepartmentId(department.getDepartmentId()))
            .collect(Collectors.toList());

        ResponseEntity<List<EmployeeDTO>> employees = restApiClient.callRestApi(
            employeeServiceProperties.getSave(),
            HttpMethod.POST,
            null,
            departmentDTO.getEmployees()
        );

        return new DepartmentDTO(department, employees.getBody());
    }

    @Transactional
    @DeleteMapping("/{departmentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDepartment(@PathVariable Integer departmentId) {
        log.info("Deleting department with id: {}", departmentId);
        departmentRepository.deleteById(departmentId);

        Map<String, Object> uriVariables = Map.of("departmentId", departmentId);

        restApiClient.callRestApi(
            employeeServiceProperties.getDeleteByDeptId(),
            HttpMethod.DELETE,
            uriVariables,
            null
        );
    }
}
