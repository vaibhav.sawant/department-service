package com.tw.department.exception;

public class DepartmentNotFound extends RuntimeException {
    public DepartmentNotFound(Integer departmentId) {
        super(String.format("Department not found with id %d", departmentId));
    }
}
