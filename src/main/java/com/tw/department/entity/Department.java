package com.tw.department.entity;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@ToString
public class Department {

    @Id
    @GeneratedValue
    private Integer departmentId;

    private String name;

    private String location;

}
