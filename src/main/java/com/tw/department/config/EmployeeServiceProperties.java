package com.tw.department.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "employee-service.urls")
public class EmployeeServiceProperties {

    private String getByDeptId;

    private String save;

    public String deleteByDeptId;
}

