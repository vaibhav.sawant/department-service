package com.tw.department.http;

import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.Map;

import static java.util.Objects.isNull;

@Component
@AllArgsConstructor
public class RestApiClient<T, R> {

    private RestTemplate restTemplate;

    public ResponseEntity<R> callRestApi(String url, HttpMethod method,  Map<String, Object> uriVariables, T requestBody) {
        if(isNull(uriVariables)) {
            uriVariables = Collections.emptyMap();
        }

        String employeeUrl = UriComponentsBuilder
            .fromHttpUrl(url)
            .buildAndExpand(uriVariables)
            .toUriString();

        HttpEntity<T> entity = new HttpEntity<>(requestBody);

        ResponseEntity<R> responseEntity = restTemplate.exchange(
            employeeUrl,
            method,
            entity,
            new ParameterizedTypeReference<>() {
            }
        );
        return responseEntity;
    }
}
